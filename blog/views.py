from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import ListView
from .models import Post


class IndexListView(ListView):
    queryset = Post.objects.all()
    context_object_name = 'blogers'
    template_name = 'index.html'


class BlogListView(ListView):
    queryset = Post.published.filter(author_id=2)
    context_object_name = 'blog'
    paginate_by = 4
    template_name = 'blog.html'

