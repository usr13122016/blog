from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse


class PublishedManager(models.Manager):
    def get_queryset(self):
        return super(PublishedManager, self).get_queryset().filter(status='опубликован')

class NotPublishedManager(models.Manager):
    def get_queryset(self):
        return super(NotPublishedManager, self).get_queryset().filter(status='добавлен')


class Post(models.Model):
    STATUS_CHOICES = (
        ('добавлен', 'Добавлен'),
        ('опубликован', 'Опубликован'),
    )
    title = models.CharField("Название", max_length=250)
    slug = models.SlugField("Слаг", max_length=250, unique_for_date='publish')
    author = models.ForeignKey(User, related_name='blog_posts', verbose_name="Автор")
    body = models.TextField("Контент")
    publish = models.DateTimeField("Дата публикация", default=timezone.now)
    created = models.DateTimeField("Дата создания", auto_now_add=True)
    updated = models.DateTimeField("Дата изменения", auto_now=True)
    status = models.CharField("Статус", max_length=11, choices=STATUS_CHOICES, default='добавлен')
    objects = models.Manager()
    published = PublishedManager()
    notpublished = NotPublishedManager()

    class Meta:
        ordering = ('-publish',)

    def __str__(self):
        return self.title


class Subscript(models.Model):
    owner = models.ForeignKey(User, related_name='blog_owner', verbose_name="Автор")
    follower = models.ForeignKey(User, related_name='blog_follower', verbose_name="Подписчик")
    created = models.DateTimeField("Дата подписка", auto_now_add=True)
    objects = models.Manager()

    class Meta:
        ordering = ('-owner',)
 