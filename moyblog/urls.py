from django.conf.urls import include, url
from django.contrib import admin
from blog.views import IndexListView, BlogListView
from blog.models import Post

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', IndexListView.as_view(), name='index'),
    url(r'^blog/$', BlogListView.as_view(), name='blog'),

]
