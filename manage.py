#!/usr/bin/env python
#!(dp) F:\Users\adm\moyblog>python

import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "moyblog.settings")
    try:
        from django.core.management import execute_from_command_line
    except ImportError:
        try:
            import django
        except ImportError:
            raise ImportError(
                "Установлено ли Django? "
                "Проверим PYTHONPATH env? "
                "Активироват virtual environment (workon dp)"
            )
        raise
    execute_from_command_line(sys.argv)
